package elements.kioli.utils;

import android.content.Context;
import android.net.ConnectivityManager;

public final class ConnectionStatusUtil {

	private ConnectionStatusUtil() {
	}

	/**
	 * Check if the device is connected to the internet
	 * 
	 * @param context context of the activity
	 * @return true if connected, false otherwise
	 */
	public static boolean hasInternetConnection(final Context context) {
		final ConnectivityManager cm = (ConnectivityManager) context.getApplicationContext().getSystemService(Context.CONNECTIVITY_SERVICE);
		if (cm.getActiveNetworkInfo() != null
				&& cm.getActiveNetworkInfo().isAvailable()
				&& cm.getActiveNetworkInfo().isConnected()) {
			return true;
		}
		return false;
	}


}
