package elements.kioli.ui.fragment;

import android.app.Fragment;
import android.content.Intent;
import android.os.Bundle;
import android.provider.Settings;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.ListView;

import java.util.ArrayList;
import java.util.List;

import elements.kioli.R;
import elements.kioli.data.CsvItemModel;
import elements.kioli.data.ResultHandler;
import elements.kioli.error.ErrorCallback;
import elements.kioli.error.ErrorDialog;
import elements.kioli.error.ErrorInfo;
import elements.kioli.error.ErrorType;
import elements.kioli.ui.IListImageClickHandler;
import elements.kioli.ui.MyAdapter;
import elements.kioli.ui.activity.PicActivity;
import elements.kioli.utils.ConnectionStatusUtil;
import elements.kioli.wiring.ClassWiring;

public class MainFragment extends BaseFragment implements View.OnClickListener, ErrorCallback, ResultHandler, IListImageClickHandler {

    private static final String FRAGMENT_DIALOG_TAG = "fragment dialog tag";

    private LinearLayout layoutError;
    private LinearLayout layoutLoading;
    private ListView list;

    private MyAdapter adapter;

    public static MainFragment newInstance() {
        return new MainFragment();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_main, container, false);
        list = (ListView) view.findViewById(R.id.list);
        layoutLoading = (LinearLayout) view.findViewById(R.id.loading_layout);
        layoutError = (LinearLayout) view.findViewById(R.id.error_layout);
        view.findViewById(R.id.button_retry).setOnClickListener(this);
        view.findViewById(R.id.button_settings).setOnClickListener(this);
        return view;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        adapter = new MyAdapter(new ArrayList<CsvItemModel>(), getActivity());
        adapter.setCallback(this);
        list.setAdapter(adapter);

        if (savedInstanceState == null) {
            getItems();
        } else {
            if (adapter.getCount() == 0) {
                showErrorLayout();
            } else {
                showListLayout();
            }
        }
    }

    private void getItems() {
        // Load images either from DB (exception if DB not found) or from server
        List<CsvItemModel> listImages = new ArrayList<>();
        try {
            listImages = CsvItemModel.listAll(CsvItemModel.class);
        } catch (Exception e) {
            e.printStackTrace();
        }

        if (listImages.size() == 0) {
            callApi();
        } else {
            showListLayout();
            adapter.setData((ArrayList<CsvItemModel>) listImages);
        }
    }

    private void callApi() {
        if (ConnectionStatusUtil.hasInternetConnection(getActivity())) {
            showLoadingLayout();
            ClassWiring.getCsvManager(this).getMapCsvFile();
        } else {
            showErrorLayout();
        }
    }

    private void showListLayout() {
        list.setVisibility(View.VISIBLE);
        layoutError.setVisibility(View.GONE);
        layoutLoading.setVisibility(View.GONE);
    }

    private void showErrorLayout() {
        list.setVisibility(View.GONE);
        layoutError.setVisibility(View.VISIBLE);
        layoutLoading.setVisibility(View.GONE);
    }

    private void showLoadingLayout() {
        list.setVisibility(View.GONE);
        layoutError.setVisibility(View.GONE);
        layoutLoading.setVisibility(View.VISIBLE);
    }

    private void createCsvErrorDialog(String message) {
        ErrorInfo info = new ErrorInfo();
        info.setTitle(getString(R.string.default_error_title));
        info.setMessage(message);
        info.setNegativeButtonText(getString(R.string.error_button_close));
        info.setNegativeButtonAction(ErrorType.CLOSE);
        info.setPositiveButtonText(getString(R.string.error_button_retry));
        info.setPositiveButtonAction(ErrorType.RETRY);
        Fragment dialog = ErrorDialog.newInstance(info, MainFragment.this);
        getFragmentManager().beginTransaction().add(dialog, FRAGMENT_DIALOG_TAG).commit();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.button_retry:
                callApi();
                break;
            case R.id.button_settings:
                startActivity(new Intent(Settings.ACTION_WIFI_SETTINGS));
                break;
            default:
                break;
        }
    }

    @Override
    public void onRetry() {
        callApi();
    }

    @Override
    public void onClose() {
        getActivity().finish();
    }

    @Override
    public void onRightResult(ArrayList<CsvItemModel> data) {
        showListLayout();
        adapter.setData(data);
    }

    @Override
    public void onErrorResult(String error) {
        createCsvErrorDialog(error);
    }

    @Override
    public void onListRowImageClicked(int position) {
        startActivity(PicActivity.getStartIntent(getActivity(), null, adapter.getItem(position)));
    }
}
