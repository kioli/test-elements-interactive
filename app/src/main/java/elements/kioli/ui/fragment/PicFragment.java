package elements.kioli.ui.fragment;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;

import elements.kioli.R;
import elements.kioli.components.SquareImageView;
import elements.kioli.data.CsvItemModel;

public class PicFragment extends BaseFragment {

    private static final String EXTRA_CSV_ENTRY = "extra csv entry";

    private TextView title;
    private TextView description;
    private ProgressBar progress;
    private SquareImageView image;

    public static PicFragment newInstance(final Bundle args) {
        final PicFragment fragment = new PicFragment();
        fragment.setArguments(args);
        return fragment;
    }

    public static Bundle createFragmentParams(Bundle args, final CsvItemModel item) {
        if (args == null) {
            args = new Bundle();
        }
        args.putParcelable(EXTRA_CSV_ENTRY, item);
        return args;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_pic, container, false);
        title = (TextView) view.findViewById(R.id.image_title);
        description = (TextView) view.findViewById(R.id.image_description);
        progress = (ProgressBar) view.findViewById(R.id.progress);
        image = (SquareImageView) view.findViewById(R.id.image);
        return view;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        CsvItemModel model = getArguments().getParcelable(EXTRA_CSV_ENTRY);
        title.setText(model.getTitle());
        description.setText(model.getDescription());
        Picasso.with(getActivity()).load(model.getImage()).noFade().into(image, new Callback() {
            @Override
            public void onSuccess() {
                progress.setVisibility(View.GONE);
            }

            @Override
            public void onError() {
                image.setImageResource(R.mipmap.error);
            }
        });
    }

    @Override
    public void onDetach() {
        super.onDetach();
        Picasso.with(getActivity()).cancelRequest(image);
    }
}
