package elements.kioli.ui.activity;

import android.app.Fragment;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;

import elements.kioli.data.CsvItemModel;
import elements.kioli.ui.activity.common.BaseActivity;
import elements.kioli.ui.fragment.PicFragment;


public class PicActivity extends BaseActivity {

    public static Intent getStartIntent(final Context context, Bundle args, final CsvItemModel item) {
        final Intent intent = new Intent(context, PicActivity.class);
        args = PicFragment.createFragmentParams(args, item);
        intent.putExtra(EXTRA_BUNDLE, args);
        return intent;
    }

    @Override
    protected Fragment getFragment(final Bundle args) {
        return PicFragment.newInstance(args);
    }

}
