package elements.kioli.ui.activity;

import android.app.Fragment;
import android.os.Bundle;

import elements.kioli.ui.activity.common.BaseActivity;
import elements.kioli.ui.fragment.MainFragment;

public class MainActivity extends BaseActivity {

    @Override
    protected Fragment getFragment(Bundle args) {
        return MainFragment.newInstance();
    }
}
