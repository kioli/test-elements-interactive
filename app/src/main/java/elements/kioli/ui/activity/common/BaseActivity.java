package elements.kioli.ui.activity.common;

import android.app.Fragment;
import android.app.FragmentTransaction;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.util.Log;
import android.view.MotionEvent;

import elements.kioli.R;

public abstract class BaseActivity extends FragmentActivity {

    protected static final String EXTRA_BUNDLE = "extra_bundle";
    protected static final String FRAGMENT_TAG = "fragment_tag";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_base);

        if (savedInstanceState == null) {
            final FragmentTransaction fragmentTransaction = getFragmentManager().beginTransaction();

            final Bundle extras = getIntent().getExtras();
            final Bundle args = (extras == null) ? null : extras.getBundle(EXTRA_BUNDLE);

            final Fragment fragment = getFragment(args);
            fragmentTransaction.replace(R.id.base_frame, fragment, FRAGMENT_TAG).commit();
        }
    }

    /**
     * Get the fragment to be used in the activity
     *
     * @param args
     *            arguments to be passed to Fragment
     * @return a new Fragment instance
     */
    protected abstract Fragment getFragment(final Bundle args);

    /**
     * Counters a possible tap-jacking problem when malicious apps fire this app and then have
     * a toast placed over its screen, allowing the touch event to pass through
     *
     * http://blog.nvisium.com/2011/05/revisiting-android-tapjacking.html
     * http://developer.android.com/reference/android/view/MotionEvent.html#FLAG_WINDOW_IS_OBSCURED
     */
    @Override
    public boolean dispatchTouchEvent(final MotionEvent ev) {
        if ((ev.getFlags() & MotionEvent.FLAG_WINDOW_IS_OBSCURED) != 0) {
            Log.e(this.getLocalClassName(), "Potential malicious attack for tap jacking");
            return true;
        } else {
            return super.dispatchTouchEvent(ev);
        }
    }
}
