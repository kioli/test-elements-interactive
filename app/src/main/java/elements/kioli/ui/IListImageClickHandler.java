package elements.kioli.ui;

public interface IListImageClickHandler {
    void onListRowImageClicked(int position);
}
