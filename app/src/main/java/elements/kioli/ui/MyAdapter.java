package elements.kioli.ui;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

import elements.kioli.data.CsvItemModel;
import elements.kioli.R;

public class MyAdapter extends BaseAdapter {

    private final Context context;
    private ArrayList<CsvItemModel> data;
    private IListImageClickHandler callback;

    public MyAdapter(final ArrayList<CsvItemModel> data, final Context context) {
        this.data = data;
        this.context = context;
    }

    public void setData(ArrayList<CsvItemModel> data) {
        this.data = data;
        notifyDataSetChanged();
    }

    public void setCallback(IListImageClickHandler callback) {
        this.callback = callback;
    }

    @Override
    public int getCount() {
        return data.size();
    }

    @Override
    public CsvItemModel getItem(int position) {
        return data.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        final myHolder holder;

        if (convertView == null) {
            convertView = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_item, parent, false);
            holder = new myHolder();
            holder.image = (ImageView) convertView.findViewById(R.id.list_image);
            holder.progress = (ProgressBar) convertView.findViewById(R.id.list_progress);
            holder.title = (TextView) convertView.findViewById(R.id.item_title);
            holder.description = (TextView) convertView.findViewById(R.id.item_description);
            convertView.setTag(holder);
        } else {
            holder = (myHolder) convertView.getTag();
        }

        final CsvItemModel model = data.get(position);

        holder.title.setText(model.getTitle());
        holder.description.setText(model.getDescription());
        holder.image.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                callback.onListRowImageClicked(position);
            }
        });

        Picasso.with(context).load(model.getImage())
                .noFade().into(holder.image, new Callback() {
                    @Override
                    public void onSuccess() {
                        holder.progress.setVisibility(View.GONE);
                    }

                    @Override
                    public void onError() {
                        holder.image.setImageResource(R.mipmap.error);
                        holder.image.setOnClickListener(null);
                    }
        });
        return convertView;
    }

    private static class myHolder {
        public ImageView image;
        public ProgressBar progress;
        public TextView title;
        public TextView description;
    }
}