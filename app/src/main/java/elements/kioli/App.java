package elements.kioli;

import android.content.Context;

import com.orm.SugarApp;

import java.io.File;

import elements.kioli.wiring.ClassFactoryImpl;
import elements.kioli.wiring.ClassWiring;

public class App extends SugarApp {

    private static final String CSV_FILE_FOLDER = "csv";
    private static final String CSV_FILE_NAME = "myfile.csv";
    private static File file;

    @Override
    public void onCreate() {
        super.onCreate();
        ClassWiring.setClassFactory(new ClassFactoryImpl());

        file = new File(getDir(CSV_FILE_FOLDER, Context.MODE_PRIVATE), CSV_FILE_NAME);
    }

    public static File getFile() {
        return file;
    }
}
