package elements.kioli.error;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

public class ErrorDialog extends DialogFragment implements DialogInterface.OnClickListener {

    private static final String ERROR_INFO_KEY = "ERROR_INFO_KEY";

    private ErrorInfo errorInfo;
    private ErrorCallback actionHandler;

    public static ErrorDialog newInstance(final ErrorInfo errorInfo, final ErrorCallback actionHandler) {
        final ErrorDialog fragment = new ErrorDialog();

        final Bundle args = new Bundle();
        args.putParcelable(ERROR_INFO_KEY, errorInfo);
        fragment.setArguments(args);
        fragment.setActionHandler(actionHandler);

        return fragment;
    }

    private void setActionHandler(final ErrorCallback actionHandler) {
        this.actionHandler = actionHandler;
    }

    @Override
    public void onCreate(final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRetainInstance(true);

        final Bundle args = getArguments();
        args.setClassLoader(ErrorInfo.class.getClassLoader());
        errorInfo = args.getParcelable(ERROR_INFO_KEY);

        if (errorInfo == null) {
            dismiss();
        }
    }

    @Override
    public Dialog onCreateDialog(final Bundle savedInstanceState) {
        if (errorInfo == null) {
            return new Dialog(getActivity());
        }

        final AlertDialog.Builder builder = new AlertDialog.Builder(getActivity())
                .setTitle(errorInfo.getTitle())
                .setMessage(errorInfo.getMessage())
                .setPositiveButton(errorInfo.getPositiveButtonText(), this);

        if (errorInfo.getNegativeButtonText() != null) {
            builder.setNegativeButton(errorInfo.getNegativeButtonText(), this);
        }

        return builder.create();
    }

    @Override
    public View onCreateView(final LayoutInflater inflater, final ViewGroup container, final Bundle savedInstanceState) {
        setCancelable(errorInfo.isCancelable());
        return super.onCreateView(inflater, container, savedInstanceState);
    }

    @Override
    public void onClick(final DialogInterface dialog, final int which) {
        if (actionHandler != null) {
            ErrorType type = ErrorType.NONE;

            if (which == DialogInterface.BUTTON_POSITIVE) {
                type = errorInfo.getPositiveButtonAction();
            } else if (which == DialogInterface.BUTTON_NEGATIVE) {
                type = errorInfo.getNegativeButtonAction();
            }

            switch (type) {
                case CLOSE:
                    actionHandler.onClose();
                    dismiss();
                    break;
                case RETRY:
                    actionHandler.onRetry();
                    dismiss();
                    break;
                default:
                    break;
            }
        }
    }


    @Override
    public void dismiss() {
        super.dismiss();
        final Dialog dialog = getDialog();
        if (dialog != null && dialog.isShowing()) {
            getDialog().dismiss();
        }
        getFragmentManager().beginTransaction().hide(this).remove(this).commit();
    }

    /**
     * Workaround for reported bug:
     * https://code.google.com/p/android/issues/detail?id=17423
     */
    @Override
    public void onDestroyView() {
        if (getDialog() != null && getRetainInstance()) {
            getDialog().setDismissMessage(null);
        }
        super.onDestroyView();
    }
}
