package elements.kioli.error;


/**
 * Callback for action on error message response from the user
 */
public interface ErrorCallback {

    void onRetry();

    void onClose();
}
