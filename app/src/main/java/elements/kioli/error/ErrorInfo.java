package elements.kioli.error;

import android.os.Parcel;
import android.os.Parcelable;

public class ErrorInfo implements Parcelable {

	private String title;
	private String message;
	private String positiveButtonText;
	private String negativeButtonText;
	private ErrorType positiveButtonAction;
	private ErrorType negativeButtonAction;
	private boolean cancelable = false;

	public ErrorInfo(){}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public String getPositiveButtonText() {
		return positiveButtonText;
	}

	public void setPositiveButtonText(String positiveButtonText) {
		this.positiveButtonText = positiveButtonText;
	}

	public String getNegativeButtonText() {
		return negativeButtonText;
	}

	public void setNegativeButtonText(String negativeButtonText) {
		this.negativeButtonText = negativeButtonText;
	}

	public ErrorType getPositiveButtonAction() {
		return positiveButtonAction;
	}

	public void setPositiveButtonAction(ErrorType positiveButtonAction) {
		this.positiveButtonAction = positiveButtonAction;
	}

	public ErrorType getNegativeButtonAction() {
		return negativeButtonAction;
	}

	public void setNegativeButtonAction(ErrorType negativeButtonAction) {
		this.negativeButtonAction = negativeButtonAction;
	}

	public boolean isCancelable() {
		return cancelable;
	}

	public void setCancelable(boolean cancelable) {
		this.cancelable = cancelable;
	}

	private ErrorInfo(Parcel in) {
		title = in.readString();
		message = in.readString();
		positiveButtonText = in.readString();
		negativeButtonText = in.readString();
		positiveButtonAction = (ErrorType) in.readValue(ErrorType.class.getClassLoader());
		negativeButtonAction = (ErrorType) in.readValue(ErrorType.class.getClassLoader());
		cancelable = in.readByte() != 0x00;
	}

	@Override
	public int describeContents() {
		return 0;
	}

	@Override
	public void writeToParcel(Parcel dest, int flags) {
		dest.writeString(title);
		dest.writeString(message);
		dest.writeString(positiveButtonText);
		dest.writeString(negativeButtonText);
		dest.writeValue(positiveButtonAction);
		dest.writeValue(negativeButtonAction);
		dest.writeByte((byte) (cancelable ? 0x01 : 0x00));
	}

	@SuppressWarnings("unused")
	public static final Parcelable.Creator<ErrorInfo> CREATOR = new Parcelable.Creator<ErrorInfo>() {
		@Override
		public ErrorInfo createFromParcel(Parcel in) {
			return new ErrorInfo(in);
		}

		@Override
		public ErrorInfo[] newArray(int size) {
			return new ErrorInfo[size];
		}
	};
}
