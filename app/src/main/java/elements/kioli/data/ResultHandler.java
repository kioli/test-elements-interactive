package elements.kioli.data;

import java.util.ArrayList;

public interface ResultHandler {
    void onRightResult(ArrayList<CsvItemModel> data);

    void onErrorResult(String error);
}
