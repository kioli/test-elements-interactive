package elements.kioli.data.manager.impl;

import android.webkit.URLUtil;

import org.supercsv.cellprocessor.Optional;
import org.supercsv.cellprocessor.constraint.StrNotNullOrEmpty;
import org.supercsv.cellprocessor.ift.CellProcessor;
import org.supercsv.io.CsvBeanReader;
import org.supercsv.io.ICsvBeanReader;
import org.supercsv.prefs.CsvPreference;

import java.io.File;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.util.ArrayList;

import elements.kioli.App;
import elements.kioli.data.CsvItemModel;
import elements.kioli.data.ResultHandler;
import elements.kioli.data.dao.CsvDao;
import elements.kioli.data.manager.CsvManager;
import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;
import retrofit.mime.TypedByteArray;

public class CsvManagerImpl implements CsvManager {

    private final CsvDao csvDao;
    private final ResultHandler handler;
    private final Callback<Response> callback;

    public CsvManagerImpl(final CsvDao csvDao, final ResultHandler handler) {
        super();
        this.csvDao = csvDao;
        this.handler = handler;

        callback = new Callback<Response>() {
            @Override
            public void success(Response response, Response response2) {
                byte[] bytes = ((TypedByteArray) response.getBody()).getBytes();
                ArrayList<CsvItemModel> data = readFile(bytes);
                handler.onRightResult(data);
            }

            @Override
            public void failure(RetrofitError error) {
                handler.onErrorResult(error.getMessage());
            }
        };
    }

    @Override
    public void getMapCsvFile() {
        csvDao.retrieveCsvFile(callback);
    }

    private ArrayList<CsvItemModel> readFile(byte[] bytes) {
        File file = App.getFile();
        ArrayList<CsvItemModel> data = new ArrayList<>();
        try {
            FileOutputStream fileOutputStream = new FileOutputStream(file);
            fileOutputStream.write(bytes);
            fileOutputStream.close();
            readWithCsvBeanReader(file, data);
        } catch(Exception e) {
            handler.onErrorResult(e.getMessage());
        }
        return data;
    }

    private void readWithCsvBeanReader(File file, ArrayList<CsvItemModel> data) throws Exception {
        ICsvBeanReader beanReader = null;
        try {
            beanReader = new CsvBeanReader(new FileReader(file), CsvPreference.STANDARD_PREFERENCE);
            final String[] header = beanReader.getHeader(true);
            final CellProcessor[] processors = getProcessors();

            CsvItemModel item;
            while((item = beanReader.read(CsvItemModel.class, header, processors)) != null ) {
                CsvItemModel csvItem = new CsvItemModel();
                if (URLUtil.isValidUrl(item.getImage())) {
                    csvItem.setTitle(item.getTitle());
                    csvItem.setDescription(item.getDescription());
                    csvItem.setImage(item.getImage());

                    // Storing in the DB for hard caching
                    item.save();

                    data.add(item);
                }
            }
        }
        finally {
            if( beanReader != null ) {
                beanReader.close();
            }
        }
    }

    /**
     * Sets up the processors used for the examples.
     * title description image
     *
     * @return the cell processors
     */
    private static CellProcessor[] getProcessors() {

        final CellProcessor[] processors = new CellProcessor[] {
                new StrNotNullOrEmpty(),  // title
                new StrNotNullOrEmpty(), // description
                new Optional()  // image
        };

        return processors;
    }
}
