package elements.kioli.data.manager;

/**
 * Csv file manager
 */
public interface CsvManager {

    /**
     * Retrieve the csv file and its content
     */
    void getMapCsvFile();

}
