package elements.kioli.data.dao.impl;

import elements.kioli.data.connection.CsvApi;
import elements.kioli.data.connection.ServiceGenerator;
import elements.kioli.data.dao.CsvDao;
import retrofit.Callback;
import retrofit.client.Response;

public class CsvDaoImpl implements CsvDao {

    private static final String DOC_KEY = "0Aqg9JQbnOwBwdEZFN2JKeldGZGFzUWVrNDBsczZxLUE";
    private static final boolean DOC_SINGLE = true;
    private static final int DOC_GID = 0;
    private static final String DOC_OUTPUT = "csv";

    public CsvDaoImpl() {
    }

    @Override
    public void retrieveCsvFile(Callback<Response> callback) {
        CsvApi api = ServiceGenerator.createService(CsvApi.class);
        api.getDocCsv(DOC_KEY, DOC_SINGLE, DOC_GID, DOC_OUTPUT, callback);
    }
}
