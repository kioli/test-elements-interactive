package elements.kioli.data.dao;

import retrofit.Callback;
import retrofit.client.Response;

/**
 * Csv file dao
 */
public interface CsvDao {

    /**
     * Retrieve the csv file and its content
     */
    void retrieveCsvFile(Callback<Response> callback);

}
