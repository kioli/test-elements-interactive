package elements.kioli.data.connection;

import retrofit.RestAdapter;

public class ServiceGenerator {

    private final static String BASE_URL = "https://docs.google.com/spreadsheet";

    private ServiceGenerator() {}

    public static <S> S createService(Class<S> serviceClass) {
        RestAdapter.Builder builder = new RestAdapter.Builder()
                .setEndpoint(BASE_URL);

        RestAdapter adapter = builder.build();
        return adapter.create(serviceClass);
    }
}