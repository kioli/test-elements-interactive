package elements.kioli.data.connection;

import retrofit.Callback;
import retrofit.client.Response;
import retrofit.http.GET;
import retrofit.http.Query;

public interface CsvApi {

    /**
     * API call for retrieving the CSV document
     *
     * @param key The key of the document
     * @param single boolean value
     * @param gid integer
     * @param output format for the document
     */
    @GET("/ccc")
    void getDocCsv(@Query("key") String key, @Query("single") boolean single,
                   @Query("gid") int gid, @Query("output") String output, Callback<Response> callback);
}