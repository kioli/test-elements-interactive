package elements.kioli.data;

import android.os.Parcel;
import android.os.Parcelable;

import com.orm.SugarRecord;

public class CsvItemModel extends SugarRecord<CsvItemModel> implements Parcelable {

    private String imageUrl;
    private String title;
    private String description;

    public String getImage() {
        return imageUrl;
    }

    public void setImage(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public CsvItemModel(){}

    private CsvItemModel(Parcel in) {
        imageUrl = in.readString();
        title = in.readString();
        description = in.readString();
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(imageUrl);
        dest.writeString(title);
        dest.writeString(description);
    }

    @SuppressWarnings("unused")
    public static final Parcelable.Creator<CsvItemModel> CREATOR = new Parcelable.Creator<CsvItemModel>() {
        @Override
        public CsvItemModel createFromParcel(Parcel in) {
            return new CsvItemModel(in);
        }

        @Override
        public CsvItemModel[] newArray(int size) {
            return new CsvItemModel[size];
        }
    };
}
