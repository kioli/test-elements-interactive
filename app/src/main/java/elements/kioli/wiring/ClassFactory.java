package elements.kioli.wiring;

import elements.kioli.data.ResultHandler;
import elements.kioli.data.dao.CsvDao;
import elements.kioli.data.manager.CsvManager;

/**
 * Class factory
 */
public interface ClassFactory {

	/**
	 * Get the csv file manager
	 *
	 * @param  csvDao dao to retrieve data
	 * @param  handler to bring back the result to the entity asking for it
	 * @return CsvManager
	 */
	CsvManager getCsvManager(CsvDao csvDao, ResultHandler handler);

	/**
	 * Get the csv file manager
	 *
	 * @return CsvDao
	 */
	CsvDao getCsvDao();
}
