package elements.kioli.wiring;

import elements.kioli.data.ResultHandler;
import elements.kioli.data.dao.CsvDao;
import elements.kioli.data.manager.CsvManager;

public class ClassWiring {

    private static ClassFactory classFactory;
    private static CsvManager csvManager;
    private static CsvDao csvDao;

    private ClassWiring() {
    }

    /**
     * Set the class factory. Easy to switch to debug or mock mode
     *
     * @param classFactory factory for managers and daos
     */
    public static void setClassFactory(final ClassFactory classFactory) {
        ClassWiring.classFactory = classFactory;
        csvManager = null;
        csvDao = null;
    }

    public static CsvManager getCsvManager(ResultHandler handler) {
        if (csvManager == null) {
            csvManager = classFactory.getCsvManager(getCsvDao(), handler);
        }
        return csvManager;
    }

    private static CsvDao getCsvDao() {
        if (csvDao == null) {
            csvDao = classFactory.getCsvDao();
        }
        return csvDao;
    }
}
