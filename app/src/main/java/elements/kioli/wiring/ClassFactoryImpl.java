package elements.kioli.wiring;

import elements.kioli.data.ResultHandler;
import elements.kioli.data.dao.CsvDao;
import elements.kioli.data.dao.impl.CsvDaoImpl;
import elements.kioli.data.manager.CsvManager;
import elements.kioli.data.manager.impl.CsvManagerImpl;

public class ClassFactoryImpl implements ClassFactory {

    public ClassFactoryImpl() {
    }

    @Override
    public CsvManager getCsvManager(CsvDao csvDao, ResultHandler handler) {
        return new CsvManagerImpl(csvDao, handler);
    }

    @Override
    public CsvDao getCsvDao() {
        return new CsvDaoImpl();
    }

}
