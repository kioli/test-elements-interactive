# README #

Assignment for Elements Interactive

### General info ###

* Applications that downloads and parses a CSV file and renders its content (Image, Title and Description) on a list. Additionally it shows a full screen representation of the aforementioned informations when the user clicks on a valid image (if the URL in the CSV does not point to an image a default error image will be shown instead)
* Version 1.0
* [TEST-Elements Interactive](git@bitbucket.org:kioli/test-elements-interactive.git)

### Details ###

* Application built using Android Studio
* Libraries: Android Support (mainly for FragmentActivity), Retrofit (REST connections), Picasso (Image loading), Super-CSV (CSV parsing), SugarOrm (persistent caching in DB)

### Approach ###

* I chose an approach with empty activities holding fragments that retain their instance (i.e. when changing orientation they are kept in memory and only detached/reattached to the newly spawned activity) so to preserve the references to the API call

* Alternatively I could have used a library like Otto (or EventBus) to keep the callback for the result active and valid (but since I am new to that approach I would rather experiment first and then use it for assignment/production purposes)